import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('st-root h1')).getText();
  }

  inputNewTodo(task: string) {
    element(by.css('input')).sendKeys('task');
  }

  getAddButton() {
    return element(by.css('button'));
  }

  clickAddButton() {
    this.getAddButton().click();
  }


}
