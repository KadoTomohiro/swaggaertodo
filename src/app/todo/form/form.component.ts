import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TodoService} from '../core/services/todo.service';

@Component({
  selector: 'st-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  addForm: FormGroup;
  constructor(private fb: FormBuilder, private todoService: TodoService) {
    this.addForm = this.fb.group({
      newTodo: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  add(): void {
    if (this.addForm.invalid) {
      return;
    }
    this.todoService.addTodo({
      task: this.addForm.value['newTodo'],
      done: false
    }).subscribe(() => {
      this.addForm.reset();
    });
  }

}
