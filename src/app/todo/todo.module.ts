import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import {FormComponent} from './form/form.component';
import {ListComponent} from './list/list.component';
import {ItemComponent} from './item/item.component';
import { TodoPageComponent } from './todo-page/todo-page.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TodoRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [
    FormComponent,
    ListComponent,
    ItemComponent,
    TodoPageComponent
  ]
})
export class TodoModule { }
