import { Component, OnInit } from '@angular/core';
import {TodoClientService} from '../core/services/api/todo-client.service';
import {Observable} from 'rxjs';
import {Todo} from '../../shared/models/todo';
import {TodoService} from '../core/services/todo.service';

@Component({
  selector: 'st-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  todoList: Observable<Todo[]>;

  constructor(private service: TodoService) { }

  ngOnInit() {
    this.todoList = this.service.fetchList();
  }

}
