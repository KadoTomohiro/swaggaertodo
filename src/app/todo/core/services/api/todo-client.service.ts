import {Injectable} from '@angular/core';
import {Todo} from '../../../../shared/models/todo';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoClientService {

  url = `${environment.apiHost}/api/todos`;

  constructor(private client: HttpClient) {
  }

  list(): Observable<Todo[]> {
    return this.client.get<Todo[]>(this.url);
  }

  post(newTodo: Todo): Observable<Todo> {
    return this.client.post<Todo>(this.url, newTodo);
  }
}
