import {Injectable} from '@angular/core';
import {TodoClientService} from './api/todo-client.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {Todo} from '../../../shared/models/todo';
import {switchMap, tap} from 'rxjs/operators';
import {getBootstrapListener} from '@angular/router/src/router_module';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private _todo: BehaviorSubject<Todo[]>;
  private todo: Observable<Todo[]>;

  constructor(private http: TodoClientService) {
  }

  fetchList(): Observable<Todo[]> {
    return this.http.list()
      .pipe(
        switchMap((todos: Todo[]) => {
          if (this._todo) {
            this._todo.next(todos);
          } else {
            this._todo = new BehaviorSubject<Todo[]>(todos);
            this.todo = this._todo.asObservable();
          }
          return this.todo;
        })
      );
  }

  addTodo(todo: Todo): Observable<Todo[]> {
    return this.http.post(todo)
      .pipe(
        switchMap(() => {
          return this.fetchList();
        })
      );
  }

}
