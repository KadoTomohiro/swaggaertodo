import {Component, Input, OnInit} from '@angular/core';
import {Todo} from '../../shared/models/todo';

@Component({
  selector: 'st-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() todo: Todo;
  constructor() { }

  ngOnInit() {
  }

}
