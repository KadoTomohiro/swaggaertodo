import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TodoClientService} from '../todo/core/services/api/todo-client.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
})
export class CoreModule { }
